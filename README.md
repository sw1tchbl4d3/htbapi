# htbapi
An unofficial API Wrapper for Hackthebox (and more!) 

Wanna know more? Wanna know what functions exist?
Go to the [Wiki](https://gitlab.com/sw1tchbl4d3/htbapi/-/wikis/)

## How to use the API
You can:
* Install the library
* Just keep it in your working directory.

### Installing - Way 1:
* Clone this repository
* Do `pip3 install .` or `pip install .` whilst in the repo dir to install.

### Installing - Way 2:
* Do `pip3 install htbapi` or `pip install htbapi` to install.

## Usage:

You have 4 sub-libraries you can import.

`htbapi.core`, `htbapi.machines`, `htbapi.challenges` and `htbapi.social`

If you wanna know what functions you can use, go to the [Wiki](https://gitlab.com/sw1tchbl4d3/htbapi/-/wikis/).

***

If you really like my work and you have some spare money to spend, why not [donate on PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=U2BJNHKZYRR38&source=url)? 
